import * as React from 'react';
import { Text, View, SafeAreaView, Fragment } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from '@expo/vector-icons/Ionicons';
import { HomePage, HomeScreen } from './pages/HomePage';
import { SearchScreen } from './pages/SearchScreen';
import { Details } from './pages/Details';
import { createStackNavigator } from '@react-navigation/stack';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function List(){
  return (
        <Stack.Navigator>
            <Stack.Screen
            name="Personnages" 
            component={HomePage}
            options={{
              headerStyle: {
                backgroundColor: 'rgb(17, 19, 27)',
                shadowOpacity: 0,
                height: 0
              },
              headerTitleStyle: {
              },
              headerTintColor: '#f7c15d',
            }}
             />
            <Stack.Screen
            name="Détails" 
            component={Details}
            options={{
              headerStyle: {
                backgroundColor: 'rgb(17, 19, 27)',
                height: 60,
              },
              headerTitleStyle: {
              },
              headerTintColor: '#f7c15d',
            }}
             />
        </Stack.Navigator>
    );
}

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Accueil') {
                iconName = focused
                  ? 'ios-list'
                  : 'ios-list-outline';
              } else if (route.name === 'Recherche') {
                iconName = focused ? 'ios-search' : 'ios-search-outline';
              }

              return <Ionicons name={iconName} size={size} color={color} />;
            },
          tabBarActiveTintColor: '#f7c15d',
          tabBarInactiveTintColor: 'gray',
          tabBarStyle: {backgroundColor: 'rgb(17, 19, 27)'}
          })}
        >
        <Tab.Screen
        name="Accueil"
        component={List}
        options={{
              headerStyle: {
                backgroundColor: 'rgb(17, 19, 27)',
                height: 120,
            
              },
              headerTitleStyle: {
                fontSize:  25,
              },
              headerTintColor: 'white',
            }} />
        <Tab.Screen
        name="Recherche"
        component={SearchScreen}
        options={{
              headerStyle: {
                backgroundColor: 'rgb(17, 19, 27)',
                height: 120,
            
              },
              headerTitleStyle: {
                fontSize:  25,
              },
              headerTintColor: 'white',
            }} />
      </Tab.Navigator>
    </NavigationContainer>
    
  );
}