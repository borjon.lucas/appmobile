import * as React from 'react';
import axios from "axios";

export class CharacterList extends React.Component {

  state = {
    popular: this.getList
  }


  async getList() {
      const url= getAPIUrl('https://breakingbadapi.com/api/characters')
      const list = await axios.get(url).then(response => {
          return response.data.results
      })
        this.setState({list})
    }
}

const baseUrl = 'https://breakingbadapi.com';

export function getList() {
  return fetch(`${baseUrl}/api/characters`)
    .then(data => data.json())
}