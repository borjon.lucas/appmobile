import axios from 'axios';
import * as React from 'react';
import {useState, useEffect} from 'react';
import { TouchableOpacity, ImageBackground, ScrollView, View, Image, Text, StyleSheet, FlatList, StatusBar, TextInput} from 'react-native';
import { withSafeAreaInsets } from 'react-native-safe-area-context';

export class SearchScreen extends React.Component {

    state = {
    list: this.getList()
  }

  async getList() {
      const url = 'https://thronesapi.com/api/v2/characters'
      const list = await axios.get(url).then(response => {
          return response.data
      })
        this.setState({list})
    }

  
  async searchResults(param) {
    console.log(param)
    const list = await this.state.list;
    console.log('liste :', list)
    const newList= list.filter(element=>element.firstName.includes(param))
    console.log('newliste :', newList)
    param == "" ? this.setState({list: await this.getList()}) : this.setState({list:newList})
  }

  render() {
    const data = this.state.list
    //console.log("test", data)

  return (
    <ScrollView style={styles.body}>
        <TextInput
          style={styles.input}
          placeholder="Recherche"
          onChangeText={(param) => this.searchResults(param)}
          value={this.state.text}
          placeholderTextColor="grey"
        />
        <View style={styles.container}>
          {
            Array.from(data).map((character) => {
              return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Détails", {id:character.id})} key={character.id} style={styles.card}>
              <ImageBackground source={{uri: character.imageUrl}}  resizeMode="cover" style={styles.thumbnail}/>
              <Text>{character.firstname}</Text>
            </TouchableOpacity>
              )
            })
          }
      </View>
    </ScrollView>
  );
}


}


const styles = StyleSheet.create({
  input: {
    height: 50,
    color: 'white',
    borderColor: 'white',
    borderWidth: 0.5,
    paddingLeft: 20,
    borderRadius: '100%',
    marginTop: 20,
    marginBottom: 10,
    marginRight: 20,
    marginLeft: 20,
    fontStyle: 'italic',
  },
  thumbnail: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  logo: {
    height: 150,
    width: '100%',
    marginTop: 10,
    marginBottom: 0,
  },

  card: {
   width: 180, 
   height: 300, 
   display: 'flex',
   margin: 10,
   flexWrap: 'wrap',
  },

  container: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  body: {
    backgroundColor: 'rgb(17, 19, 27)',
    display: 'flex',
    textAlign: 'center',
  }

})
