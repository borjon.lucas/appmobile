import axios from 'axios';
import * as React from 'react';
import {useState, useEffect} from 'react';
import { ImageBackground, ScrollView, View, Image, Text, StyleSheet, FlatList} from 'react-native';

export class Details extends React.Component {

  state = {
    list: this.getList(),
    id :  this.props.route.params.id
  }

  async getList() {
      const id = this.props.route.params.id
      const url = 'https://thronesapi.com/api/v2/characters/' + id
      const list = await axios.get(url).then(response => {
          return response.data
      })
        this.setState({list})

    }

  render() {
    id = this.state.id
    console.log(id)
    const data = this.state.list
    console.log(data)

  return (
    <ScrollView style={styles.body}>
        <View style={styles.container}>
              <Image source={{uri: data.imageUrl}}  resizeMode="cover" style={styles.thumbnail}/>
              <Text style={styles.txt}>Identité : {data.firstName} {data.lastName}</Text>
              <Text style={styles.txt}>Famille : {data.family}</Text>
              <Text style={styles.txt}>Titre : {data.title}</Text>
      </View>
    </ScrollView>

  );
}


}


const styles = StyleSheet.create({
  thumbnail: {
    width: 500,
    height: 500,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: 30,
  },
  logo: {
    height: 100,
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
  },

  container: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    color: '#fffff',
  },
  body: {
    backgroundColor: 'rgb(17, 19, 27)',
    display: 'flex',
    color: 'white'
  },
  txt: {
    fontSize: 20,
    color: 'white',
  }

})
