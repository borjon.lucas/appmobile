import axios from 'axios';
import * as React from 'react';
import {useState, useEffect} from 'react';
import { TouchableOpacity, ImageBackground, ScrollView, View, Image, Text, StyleSheet, FlatList, StatusBar} from 'react-native';

export class HomePage extends React.Component {

  state = {
    list: this.getList()
  }

  async getList() {
      const url = 'https://thronesapi.com/api/v2/characters'
      const list = await axios.get(url).then(response => {
          return response.data
      })
        this.setState({list})
    }

  render() {
    const data = this.state.list
    console.log("test", data)

  return (
    <ScrollView style={styles.body}>
        <Image style={styles.logo} source = {require('../assets/logo.png')} resizeMode={'contain'} />
        <View style={styles.container}>
          {
            Array.from(data).map((character) => {
              return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Détails", {id:character.id})} key={character.id} style={styles.card}>
              <ImageBackground source={{uri: character.imageUrl}}  resizeMode="cover" style={styles.thumbnail}/>
              <Text>{character.firstname}</Text>
            </TouchableOpacity>
              )
            })
          }
      </View>
    </ScrollView>
  );
}


}


const styles = StyleSheet.create({
  thumbnail: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  logo: {
    height: 150,
    width: '100%',
    marginTop: 10,
    marginBottom: 0,
  },

  card: {
   width: 180, 
   height: 300, 
   display: 'flex',
   margin: 10,
   flexWrap: 'wrap',
  },

  container: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  body: {
    backgroundColor: 'rgb(17, 19, 27)',
    display: 'flex',
  }

})
